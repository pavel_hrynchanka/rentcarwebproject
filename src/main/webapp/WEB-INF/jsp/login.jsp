<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">

    <title>Login</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <%--<link href="html/css/signin.css" rel="stylesheet">--%>

<body>
<%--

<div class="container">

    <form class="form-signin" role="form" method="post" action="controller">
        <h2 class="form-signin-heading">Please sign in</h2>
        <input type="email" class="form-control" placeholder="Email address" required autofocus>
        <input type="password" class="form-control" placeholder="Password" required>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
    </form>

</div>
--%>
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Login</h4>
            </div>
            <div class="modal-body">
                <form class="form-signin" role="form">
                    <h2 class="form-signin-heading">Please sign in</h2>
                    <input type="email" class="form-control" placeholder="Email address" required autofocus>
                    <input type="password" class="form-control" placeholder="Password" required>
                    <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

</body>
</html>

<%--
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title></title>
    <c:set var="locale"
           value="${not empty param.locale ? param.locale : not empty locale ? locale : pageContext.request.locale}"
           scope="session"/>
    <fmt:setLocale value="${locale}" scope="session"/>
    <fmt:setBundle basename="property"/>
</head>
<body>
<form method="post" action="controller">
    <label><fmt:message key="label.login"/><input type="text" name="login"></label><br/>
    <label><fmt:message key="label.password"/><input type="password" name="password"></label><br/>
    <input type="hidden" name="command" value="login">
    <input type="submit" value=<fmt:message key="label.enter"/>>
</form>
<form method="post" action="registration.jsp">
    <input type="hidden" name="command" value="registration">
    <input type="submit" value=<fmt:message key="label.registration"/>>
</form>

<form method="post" action="index.jsp">
    <input type="hidden" name="locale" value="ru_RUS"/>
    <input type="submit" value=<fmt:message key="label.locale.ru"/>>
</form>
<form method="post" action="index.jsp">
    <input type="hidden" name="locale" value="en_US"/>
    <input type="submit" value=<fmt:message key="label.locale.en"/>>
</form>
<br/>
<li><a href="index.jsp?locale=ru_RUS"><fmt:message key="label.locale.ru"/></a></li>
<li><a href="index.jsp?locale=en_US"> <fmt:message key="label.locale.en"/></a></li>
</body>
</html>

--%>