<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>RentCarCompany</title>
    <link rel="stylesheet" href="css/client_theme.css">
    <link rel="stylesheet" href="css/footer.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="shortcut icon" href="img/logo.jpg" type="image"/>
    <c:set var="locale"
           value="${not empty param.locale ? param.locale : not empty locale ? locale : pageContext.request.locale}"
           scope="session"/>
    <fmt:setLocale value="${locale}" scope="session"/>
    <fmt:setBundle basename="property"/>
</head>
<body>
<div class="navigation">
    <div class="container">
        <div class="row">
            <div class="navbar-header">
                <a class="navbar-brand logo" href="#">Rent Car Company</a>
            </div>
        </div>
    </div>
</div>

<div class="banner">
    <div class="container-fluid">
        <div class="banner-contentarea">
            <div class="banner-content text-center text-white">
                <div class="container-fluid text-center">
                    <div class="row content">
                        <div class="col-sm-2 sidebar-nav">
                            <ul>
                                <li><a href="#">Просмотреть машины</a></li>
                                <li><a href="#">Сделать заказ</a></li>
                                <li><a href="#">Управление заказами</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-10 text-left">
                            <h1>Welcome</h1>

                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                                ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                                ullamco laboris nisi ut aliquip ex ea commodo consequat. Excepteur sint occaecat
                                cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum
                                consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna
                                aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
                                aliquip ex ea commodo consequat.</p>
                            <hr>
                            <h3>Test</h3>

                            <p>Lorem ipsum...</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<c:import url="/WEB-INF/jsp/footer.jsp"/>
<script src="html/js/jquery-2.1.4.js"></script>
<script src="html/js/bootstrap.min.js"></script>

</body>
</html>