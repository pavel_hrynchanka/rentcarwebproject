<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title></title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/footer.css" rel="stylesheet">
</head>
<body>
<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-2 text-left">
                <ul>
                    <li><a href="?locale=en_US" title="Englsih"><img src="img/en.png"></a></li>
                    <li><a href="?locale=ru_RUS" title="Русский"><img src="img/rus.jpg"></a></li>
                </ul>
            </div>
            <div class="col-sm-8 text-center">
                <p>&copy; RentCar 2015</p>
            </div>
        </div>
    </div>
</footer>
</body>
</html>
