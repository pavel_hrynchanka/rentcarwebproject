<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>RentCarCompany</title>
    <link rel="stylesheet" href="css/index_theme.css">
    <link rel="stylesheet" href="css/footer.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="shortcut icon" href="img/logo.jpg" type="image"/>
    <c:set var="locale"
           value="${not empty param.locale ? param.locale : not empty locale ? locale : pageContext.request.locale}"
           scope="session"/>
    <fmt:setLocale value="${locale}" scope="session"/>
    <fmt:setBundle basename="property"/>
</head>
<body>
<div class="navigation">
    <div class="container">
        <div class="row">
            <div class="navbar-header">
                <a class="navbar-brand logo" href="#">Rent Car Company</a>
            </div>
        </div>
    </div>
</div>

<div class="banner">
    <div class="container">
        <div class="banner-contentarea">
            <div class="banner-content text-center text-white">
                <h1><fmt:message key="index.welcom"/></h1>
                <p><fmt:message key="index.connect"/></p>
                <p>
                    <a href="#loginModal" class="btn btn-theme" data-toggle="modal" data-target="#loginModal"><fmt:message key="index.login"/></a>
                    <a href="#registrateModal" class="btn btn-theme" data-toggle="modal" data-target="#registrateModal"><fmt:message key="index.registrate"/></a>
                </p>
            </div>
        </div>
    </div>
</div>
<%--Modal Login--%>
<div id="loginModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h2 class="form-signin-heading"><fmt:message key="modal.header.login"/></h2>
            </div>
            <div class="modal-body">
                <form class="form-signin" role="form" method="post" action="controller">
                    <input type="text" class="form-control" placeholder="<fmt:message key="modal.email"/>">
                    <input type="password" class="form-control" placeholder="<fmt:message key="modal.pwd"/>" required>
                    <input type="hidden" name="command" value="login">
                    <button class="btn btn-lg btn-primary btn-block btn-modal" type="submit"><fmt:message key="modal.sign"/></button>
                </form>
            </div>
        </div>
    </div>
</div>
<%--Modal Registrate--%>
<div id="registrateModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h2 class="modal-title"><fmt:message key="modal.header.registrate"/></h2>
            </div>
            <div class="modal-body">
                <form  method="post" action="controller">
                    <input type="text" class="form-control" placeholder="<fmt:message key="modal.name"/>" required>
                    <input type="text" class="form-control" placeholder="<fmt:message key="modal.lastName"/>" required>
                    <input type="tel" class="form-control" placeholder="<fmt:message key="modal.phone"/>" required>
                    <input type="email" class="form-control" placeholder="<fmt:message key="modal.email"/>" required>
                    <input type="number" class="form-control" placeholder="<fmt:message key="modal.pnumber"/>" required>

                    <input type="hidden" name="command" value="registration">
                    <button class="btn btn-lg btn-primary btn-block btn-modal" type="submit"><fmt:message key="modal.btn.registrate"/></button>
                </form>
            </div>
        </div>
    </div>
</div>

<c:import url="WEB-INF/jsp/footer.jsp"/>
<script src="html/js/jquery-2.1.4.js"></script>
<script src="html/js/bootstrap.min.js"></script>

</body>
</html>