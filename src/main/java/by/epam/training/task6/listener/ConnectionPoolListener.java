package by.epam.training.task6.listener;

import by.epam.training.task6.dao.connectionpool.ConnectionPool;
import by.epam.training.task6.dao.connectionpool.ConnectionPoolException;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class ConnectionPoolListener implements ServletContextListener {
    private ConnectionPool pool;

    public ConnectionPool getPool() {
        return pool;
    }

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        pool = ConnectionPool.getInstance();
        try {
            pool.init();
        } catch (ConnectionPoolException e) {
            throw new RuntimeException("Application is not availible. Problem with data source initialization");
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        pool = ConnectionPool.getInstance();
        try {
            pool.destroy();
        } catch (ConnectionPoolException e) {
            throw new RuntimeException("Application is not availible. Problem with data source closing");
        }
    }
}
