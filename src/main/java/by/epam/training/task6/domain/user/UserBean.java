package by.epam.training.task6.domain.user;

import java.io.Serializable;

public class UserBean implements Serializable {
    private int id;
    private String name;
    private String lastName;
    private String phone;
    private String email;
    private String passportNumber;
    private String role;

    public UserBean() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassportNumber() {
        return passportNumber;
    }

    public void setPassportNumber(String passportNumber) {
        this.passportNumber = passportNumber;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (getClass() != object.getClass()) {
            return false;
        }
        UserBean other = (UserBean) object;
        if (id != other.getId()) {
            return false;
        }
        if (name != other.getName()) {
            return true;
        } else {
            if (!name.equals(other.getName())) {
                return false;
            }
        }
        if (lastName == other.getLastName()) {
            return true;
        } else {
            if (!lastName.equals(other.getLastName())) {
                return false;
            }
        }
        if (phone == other.getPhone()) {
            return true;
        } else {
            if (!phone.equals(other.getPhone())) {
                return false;
            }
        }
        if (email == other.getEmail()) {
            return true;
        } else {
            if (!email.equals(other.getEmail())) {
                return false;
            }
        }
        if (passportNumber == other.getPassportNumber()) {
            return true;
        } else {
            if (!passportNumber.equals(other.getPassportNumber())) {
                return false;
            }
        }
        if (role == other.getRole()) {
            return true;
        } else {
            if (!role.equals(other.getRole())) {
                return false;
            }
        }
        return true;
    }

    @Override
    public int hashCode() {
        int result = 0;
        result = 37 * id + name.hashCode() + lastName.hashCode() + phone.hashCode();
        result += 17 * email.hashCode() + passportNumber.hashCode() + role.hashCode();
        return (int) result;
    }

    @Override
    public String toString() {
        StringBuilder buffer = new StringBuilder();
        buffer.append(getClass().getSimpleName());
        buffer.append(": id ");
        buffer.append(id);
        buffer.append(" name ");
        buffer.append(name);
        buffer.append(" lastName ");
        buffer.append(lastName);
        buffer.append(" phone ");
        buffer.append(phone);
        buffer.append(" email ");
        buffer.append(email);
        buffer.append(" passportNumber ");
        buffer.append(passportNumber);
        buffer.append(" role ");
        buffer.append(role);
        return buffer.toString();
    }

}
