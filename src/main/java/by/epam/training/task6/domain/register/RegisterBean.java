package by.epam.training.task6.domain.register;

import by.epam.training.task6.domain.order.OrderBean;
import by.epam.training.task6.domain.user.UserBean;

import java.sql.Timestamp;

public class RegisterBean {
    private int id;
    private UserBean user;
    private OrderBean order;
    private Timestamp fixingDate;
    private Timestamp returnDate;
    private boolean damage;
    private double recount;

    public RegisterBean() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public UserBean getUser() {
        return user;
    }

    public void setUser(UserBean user) {
        this.user = user;
    }

    public OrderBean getOrder() {
        return order;
    }

    public void setOrder(OrderBean order) {
        this.order = order;
    }

    public Timestamp getFixingDate() {
        return fixingDate;
    }

    public void setFixingDate(Timestamp fixingDate) {
        this.fixingDate = fixingDate;
    }

    public Timestamp getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Timestamp returnDate) {
        this.returnDate = returnDate;
    }

    public boolean isDamage() {
        return damage;
    }

    public void setDamage(boolean damage) {
        this.damage = damage;
    }

    public double getRecount() {
        return recount;
    }

    public void setRecount(double recount) {
        this.recount = recount;
    }

    @Override
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (this == object) {
            return true;
        }

        if (getClass() != object.getClass()) {
            return false;
        }

        RegisterBean other = (RegisterBean) object;
        if (id != other.getId()) {
            return false;
        }
        if (!(damage && other.isDamage())) {
            return false;
        }
        if (recount != other.getRecount()) {
            return false;
        }
        if (user == other.getUser()) {
            return true;
        } else {
            if (!user.equals(other.getUser())) {
                return false;
            }
        }
        if (order == other.getOrder()) {
            return true;
        } else {
            if (!order.equals(other.getOrder())) {
                return false;
            }
        }
        if (fixingDate == other.getFixingDate()) {
            return true;
        } else {
            if (!fixingDate.equals(other.getFixingDate())) {
                return false;
            }
        }
        if (returnDate == other.getReturnDate()) {
            return true;
        } else {
            if (!returnDate.equals(other.getReturnDate())) {
                return false;
            }
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = 0;
        result = (int) (17 * id + 47 * recount + (damage ? 11 : 0));
        result += (int) (user.hashCode() + order.hashCode() + fixingDate.hashCode() + returnDate.hashCode());
        return (int) result;
    }

    @Override
    public String toString() {
        StringBuilder buffer = new StringBuilder();
        buffer.append(getClass().getSimpleName());
        buffer.append(": id ");
        buffer.append(id);
        buffer.append(" user ");
        buffer.append(user);
        buffer.append(" order ");
        buffer.append(order);
        buffer.append(" fixingDate ");
        buffer.append(fixingDate);
        buffer.append(" returnDate ");
        buffer.append(returnDate);
        buffer.append(" damage ");
        buffer.append(damage);
        buffer.append(" recount ");
        buffer.append(recount);
        return buffer.toString();
    }
}
