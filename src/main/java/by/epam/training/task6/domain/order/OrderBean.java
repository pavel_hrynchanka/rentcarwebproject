package by.epam.training.task6.domain.order;

import by.epam.training.task6.domain.car.CarBean;
import by.epam.training.task6.domain.user.UserBean;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

public class OrderBean implements Serializable {
    private int id;
    private UserBean user;
    private CarBean car;
    private Date period;
    private Timestamp currentDate;
    private double totalSum;
    private boolean paid;
    private boolean valid;
    private String comment;

    public OrderBean() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public UserBean getUser() {
        return user;
    }

    public void setUser(UserBean user) {
        this.user = user;
    }

    public CarBean getCar() {
        return car;
    }

    public void setCar(CarBean car) {
        this.car = car;
    }

    public Date getPeriod() {
        return period;
    }

    public void setPeriod(Date period) {
        this.period = period;
    }

    public Timestamp getCurrentDate() {
        return currentDate;
    }

    public void setCurrentDate(Timestamp currentDate) {
        this.currentDate = currentDate;
    }

    public double getTotalSum() {
        return totalSum;
    }

    public void setTotalSum(double totalSum) {
        this.totalSum = totalSum;
    }

    public boolean isPaid() {
        return paid;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (this == object) {
            return true;
        }

        if (getClass() != object.getClass()) {
            return false;
        }

        OrderBean other = (OrderBean) object;
        if (id != other.getId()) {
            return false;
        }
        if (totalSum != other.getTotalSum()) {
            return false;
        }
        if (!(paid && other.isPaid())) {
            return false;
        }
        if (!(valid && other.isValid())) {
            return false;
        }
        if (user == other.getUser()) {
            return true;
        } else {
            if (!user.equals(other.getUser())) {
                return false;
            }
        }
        if (car == other.getCar()) {
            return true;
        } else {
            if (!car.equals(other.getCar())) {
                return false;
            }
        }
        if (period == other.getPeriod()) {
            return true;
        } else {
            if (!period.equals(other.getPeriod())) {
                return false;
            }
        }
        if (currentDate == other.getCurrentDate()) {
            return true;
        } else {
            if (!currentDate.equals(other.getCurrentDate())) {
                return false;
            }
        }
        if (comment == other.getComment()) {
            return true;
        } else {
            if (!comment.equals(other.getComment())) {
                return false;
            }
        }
        return true;
    }

    @Override
    public int hashCode() {
        int result = 0;
        result = (int) (17 * id + 37 * user.hashCode() + 11 * car.hashCode() + 13 * totalSum);
        result += (int) (period.hashCode() + currentDate.hashCode() + comment.hashCode());
        result += (int) ((paid ? 11 : 0) + (valid ? 13 : 47));
        return (int) result;
    }

    @Override
    public String toString() {
        StringBuilder buffer = new StringBuilder();
        buffer.append(getClass().getSimpleName());
        buffer.append(": id ");
        buffer.append(id);
        buffer.append(" user ");
        buffer.append(user);
        buffer.append(" car ");
        buffer.append(car);
        buffer.append(" period ");
        buffer.append(period);
        buffer.append(" currentDate ");
        buffer.append(currentDate);
        buffer.append(" totalSum ");
        buffer.append(totalSum);
        buffer.append(" paid ");
        buffer.append(paid);
        buffer.append(" valid ");
        buffer.append(valid);
        buffer.append(" comment ");
        buffer.append(comment);
        return buffer.toString();
    }
}
