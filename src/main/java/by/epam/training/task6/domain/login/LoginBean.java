package by.epam.training.task6.domain.login;

import by.epam.training.task6.domain.user.UserBean;

public class LoginBean {
    private int id;
    private UserBean user;
    private String login;
    private String password;

    public LoginBean() {
        user = new UserBean();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public UserBean getUser() {
        return user;
    }

    public void setUser(UserBean user) {
        this.user = user;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (this == object) {
            return true;
        }

        if (getClass() != object.getClass()) {
            return false;
        }

        LoginBean other = (LoginBean) object;
        if (id != other.getId()) {
            return false;
        }
        if (!user.equals(other)) {
            return false;
        }
        if (login == other.getLogin()) {
            return true;
        } else {
            if (!login.equals(other.getLogin())) {
                return false;
            }
        }
        if (password == other.getPassword()) {
            return true;
        } else {
            if (!password.equals(other.getPassword())) {
                return false;
            }
        }
        return true;
    }

    @Override
    public int hashCode() {
        int result = 0;
        result = 17 * id + user.hashCode() + login.hashCode() + password.hashCode();
        return (int) result;
    }

    @Override
    public String toString() {
        StringBuilder buffer = new StringBuilder();
        buffer.append(getClass().getSimpleName());
        buffer.append(": id ");
        buffer.append(id);
        buffer.append(" user ");
        buffer.append(user.toString());
        buffer.append(" login ");
        buffer.append(login);
        buffer.append(" password ");
        buffer.append(password);
        return buffer.toString();
    }
}
