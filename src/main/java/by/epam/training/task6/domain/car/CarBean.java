package by.epam.training.task6.domain.car;

import java.io.Serializable;

public class CarBean implements Serializable {
    private int id;
    private String name;
    private int capacity;
    private double power;
    private double price;
    private boolean free;

    public CarBean() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public double getPower() {
        return power;
    }

    public void setPower(double power) {
        this.power = power;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public boolean isFree() {
        return free;
    }

    public void setFree(boolean free) {
        this.free = free;
    }

    @Override
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (this == object) {
            return true;
        }

        if (getClass() != object.getClass()) {
            return false;
        }

        CarBean other = (CarBean) object;
        if (id != other.getId()) {
            return false;
        }
        if (capacity != other.getCapacity()) {
            return false;
        }
        if (power != other.getCapacity()) {
            return false;
        }
        if (price != other.getPrice()) {
            return false;
        }
        if (!(free && other.isFree())) {
            return false;
        }
        if (name == other.getName()) {
            return true;
        } else {
            if (!name.equals(other.getName())) {
                return false;
            }
        }
        return true;
    }

    @Override
    public int hashCode() {
        int result = 0;
        result = (int) (17 * id + 37 * capacity + 11 * power + 13 * price + 127);
        result += (free ? 47 : 1) + name.hashCode();
        return (int) result;
    }

    @Override
    public String toString() {
        StringBuilder buffer = new StringBuilder();
        buffer.append(getClass().getSimpleName());
        buffer.append(": id ");
        buffer.append(id);
        buffer.append(" name ");
        buffer.append(name);
        buffer.append(" capacity");
        buffer.append(capacity);
        buffer.append(" power ");
        buffer.append(power);
        buffer.append(" price ");
        buffer.append(price);
        buffer.append(" free ");
        buffer.append(free);
        return buffer.toString();
    }
}
