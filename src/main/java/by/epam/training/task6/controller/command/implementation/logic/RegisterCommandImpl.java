package by.epam.training.task6.controller.command.implementation.logic;

import by.epam.training.task6.controller.command.Command;
import by.epam.training.task6.controller.command.CommandException;
import by.epam.training.task6.controller.command.implementation.PageManager;
import by.epam.training.task6.domain.user.UserBean;
import by.epam.training.task6.service.RegistrationServiceImpl;
import by.epam.training.task6.service.ServiceException;

import javax.servlet.http.HttpServletRequest;

public class RegisterCommandImpl implements Command {
    private final static RegistrationServiceImpl REGISTRATION_SERVICE = RegistrationServiceImpl.getInstance();
    private final static String INDEX_PAGE = "path.page.index";
    private final static String REGISTRATION_PAGE = "path.page.registration";
    private final static String NAME_PARAMETER = "name";
    private final static String LAST_NAME_PARAMETER = "lastName";
    private final static String PASSPORT_NUMBER_PARAMETER = "passportNumber";
    private final static String PHONE_PARAMETER = "phone";
    private final static String EMAIL_PARAMETER = "email";

    public String execute(HttpServletRequest request) throws CommandException, ServiceException {
        UserBean transferObject = new UserBean();
        transferObject.setName(request.getParameter(NAME_PARAMETER));
        transferObject.setLastName(request.getParameter(LAST_NAME_PARAMETER));
        transferObject.setPassportNumber(request.getParameter(PASSPORT_NUMBER_PARAMETER));
        transferObject.setPhone(request.getParameter(PHONE_PARAMETER));
        transferObject.setEmail(request.getParameter(EMAIL_PARAMETER));

        String page = PageManager.getProperty(REGISTRATION_PAGE);
        try {
            UserBean viewObject = REGISTRATION_SERVICE.executeService(transferObject);
            if (viewObject != null) {
                page = PageManager.getProperty(INDEX_PAGE);
            }
        } catch (ServiceException e) {
            throw new ServiceException("Registration exception", e);
        }
        return page;
    }

}
