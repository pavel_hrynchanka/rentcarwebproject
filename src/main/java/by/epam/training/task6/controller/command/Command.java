package by.epam.training.task6.controller.command;

import by.epam.training.task6.service.ServiceException;

import javax.servlet.http.HttpServletRequest;

public interface Command {
    String execute(HttpServletRequest request) throws CommandException, ServiceException;
}
