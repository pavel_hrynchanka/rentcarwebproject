package by.epam.training.task6.controller.command.implementation.logic;

import by.epam.training.task6.controller.command.Command;
import by.epam.training.task6.controller.command.CommandException;
import by.epam.training.task6.controller.command.implementation.PageManager;
import by.epam.training.task6.domain.login.LoginBean;
import by.epam.training.task6.domain.user.UserBean;
import by.epam.training.task6.service.LoginServiceImpl;
import by.epam.training.task6.service.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class LoginCommandImpl implements Command {
    private final static LoginServiceImpl LOGIN_SERVICE = LoginServiceImpl.getInstance();
    private final static String MAIN_PAGE = "path.page.main";
    private final static String REGISTRATION_PAGE = "path.page.registration";
    private final static String LOGIN_PAGE = "path.page.login";
    private final static String LOGIN_PARAMETER = "login";
    private final static String SESSION_ATTRIBUTE = "userBean";
    private final static String PASSWORD_PARAMETER = "password";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String login = request.getParameter(LOGIN_PARAMETER);
        String password = request.getParameter(PASSWORD_PARAMETER);
        LoginBean tranferLoginObject = new LoginBean();
        tranferLoginObject.setLogin(login);
        tranferLoginObject.setPassword(password);
        String page = LOGIN_PAGE;
        try {
            UserBean userBean = LOGIN_SERVICE.executeService(tranferLoginObject);
            if (userBean != null) {
                HttpSession session =  request.getSession(true);

                session.setAttribute(SESSION_ATTRIBUTE,userBean);
                page = PageManager.getProperty(MAIN_PAGE);
            } else {
                page = PageManager.getProperty(REGISTRATION_PAGE);
            }
        } catch (ServiceException e) {
            throw new CommandException("Service Login problem", e);
        }
        return page;
    }

}
