package by.epam.training.task6.controller.command.implementation.logic;

import by.epam.training.task6.controller.command.Command;
import by.epam.training.task6.controller.command.CommandException;
import by.epam.training.task6.controller.command.implementation.PageManager;

import javax.servlet.http.HttpServletRequest;

public class LogoutCommandImpl implements Command {
    private final static String INDEX_PAGE = "path.page.index";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        request.getSession(true).invalidate();
        return PageManager.getProperty(INDEX_PAGE);
    }
}
