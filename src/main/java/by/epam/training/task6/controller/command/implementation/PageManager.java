package by.epam.training.task6.controller.command.implementation;

import java.util.ResourceBundle;

public final class PageManager {
    private final static ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle("page");

    private PageManager() {
    }

    public static String getProperty(String key) {
        return RESOURCE_BUNDLE.getString(key);
    }
}
