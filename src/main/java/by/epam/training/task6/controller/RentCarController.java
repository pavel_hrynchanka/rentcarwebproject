package by.epam.training.task6.controller;

import by.epam.training.task6.controller.command.Command;
import by.epam.training.task6.controller.command.CommandException;
import by.epam.training.task6.controller.command.CommandHelper;
import by.epam.training.task6.service.ServiceException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class RentCarController extends HttpServlet {
    private final static CommandHelper commandHelper = CommandHelper.getInstance();
    private final String COMMAND_PARAMETER="command";

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        process(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        process(request, response);
    }

    private void process(HttpServletRequest request, HttpServletResponse response) {
        String commandname = request.getParameter(COMMAND_PARAMETER);
        Command command = commandHelper.defineCommand(commandname);
        try {
            String page = command.execute(request);
            request.getRequestDispatcher(page).forward(request, response);
        } catch (CommandException e) {

        } catch (ServletException e) {

        } catch (IOException e) {

        } catch (ServiceException e) {

        }
    }
}
