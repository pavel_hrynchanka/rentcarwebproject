package by.epam.training.task6.controller.command.implementation.logic;

import by.epam.training.task6.controller.command.Command;
import by.epam.training.task6.controller.command.CommandException;
import by.epam.training.task6.controller.command.implementation.PageManager;

import javax.servlet.http.HttpServletRequest;

public class ChangeLanguageCommandImpl implements Command {
    private final static String LOCALE_PARAM = "locale";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        request.getSession(true).setAttribute(LOCALE_PARAM, request.getParameter(LOCALE_PARAM));
        return PageManager.getProperty("path.page.index");
    }
}
