package by.epam.training.task6.dao;

import by.epam.training.task6.domain.order.OrderBean;

public interface OrderDAO extends GenericDAO<OrderBean, Integer> {
}
