package by.epam.training.task6.dao;

import by.epam.training.task6.domain.car.CarBean;

public interface CarDAO extends GenericDAO<CarBean,Integer>{
}
