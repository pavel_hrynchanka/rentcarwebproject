package by.epam.training.task6.dao;

import by.epam.training.task6.domain.login.LoginBean;
import by.epam.training.task6.domain.user.UserBean;

public interface LoginDAO extends GenericDAO<LoginBean, Integer> {
    boolean checkUserLogin(LoginBean loginBean) throws DAOException;

    UserBean buildUserBean(LoginBean loginBean) throws DAOException;
}
