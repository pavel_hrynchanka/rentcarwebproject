package by.epam.training.task6.dao;

public class DAOException extends Exception{
    public DAOException(String message, Throwable cause) {
        super(message, cause);
    }

    public DAOException(String message) {
        super(message);
    }

    public DAOException() {
    }
}
