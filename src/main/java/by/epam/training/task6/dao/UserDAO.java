package by.epam.training.task6.dao;

import by.epam.training.task6.domain.user.UserBean;

public interface UserDAO extends GenericDAO<UserBean,Integer> {
    void setUserId(UserBean userBean) throws DAOException;
}