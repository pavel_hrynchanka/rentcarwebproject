package by.epam.training.task6.dao.impl;

import by.epam.training.task6.dao.DAOException;
import by.epam.training.task6.dao.OrderDAO;
import by.epam.training.task6.domain.order.OrderBean;

import java.util.List;

public class MySQLOrderDAO implements OrderDAO{
    @Override
    public OrderBean create(OrderBean entity) throws DAOException {
        return null;
    }

    @Override
    public OrderBean find(Integer key) throws DAOException {
        return null;
    }

    @Override
    public List<OrderBean> findAll() throws DAOException {
        return null;
    }

    @Override
    public boolean update(OrderBean entity) throws DAOException {
        return false;
    }

    @Override
    public boolean delete(OrderBean entity) throws DAOException {
        return false;
    }
}
