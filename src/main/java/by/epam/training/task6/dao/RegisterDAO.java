package by.epam.training.task6.dao;

import by.epam.training.task6.domain.register.RegisterBean;

public interface RegisterDAO extends GenericDAO<RegisterBean, Integer> {
}
