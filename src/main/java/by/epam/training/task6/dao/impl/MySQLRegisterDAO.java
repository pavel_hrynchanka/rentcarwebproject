package by.epam.training.task6.dao.impl;

import by.epam.training.task6.dao.DAOException;
import by.epam.training.task6.dao.RegisterDAO;
import by.epam.training.task6.domain.register.RegisterBean;

import java.util.List;

public class MySQLRegisterDAO implements RegisterDAO{


    @Override
    public RegisterBean create(RegisterBean entity) throws DAOException {

        return null;
    }

    @Override
    public RegisterBean find(Integer key) throws DAOException {
        return null;
    }

    @Override
    public List<RegisterBean> findAll() throws DAOException {
        return null;
    }

    @Override
    public boolean update(RegisterBean entity) throws DAOException {
        return false;
    }

    @Override
    public boolean delete(RegisterBean entity) throws DAOException {
        return false;
    }
}
