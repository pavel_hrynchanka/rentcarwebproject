package by.epam.training.task6.dao.impl;

import by.epam.training.task6.dao.CarDAO;
import by.epam.training.task6.dao.DAOException;
import by.epam.training.task6.domain.car.CarBean;

import java.util.List;

public class MySQLCarDAO implements CarDAO {
    @Override
    public CarBean create(CarBean entity) throws DAOException {
        return null;
    }

    @Override
    public CarBean find(Integer key) throws DAOException {
        return null;
    }

    @Override
    public List<CarBean> findAll() throws DAOException {
        return null;
    }

    @Override
    public boolean update(CarBean entity) throws DAOException {
        return false;
    }

    @Override
    public boolean delete(CarBean entity) throws DAOException {
        return false;
    }
}
