package by.epam.training.task6.dao.impl;

import by.epam.training.task6.dao.DAOException;
import by.epam.training.task6.dao.LoginDAO;
import by.epam.training.task6.dao.connectionpool.ConnectionPool;
import by.epam.training.task6.dao.connectionpool.ConnectionPoolException;
import by.epam.training.task6.domain.login.LoginBean;
import by.epam.training.task6.domain.user.UserBean;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

public class MySQLLoginDAO implements LoginDAO {
    private final static String CHECK_USER = "SELECT * FROM login WHERE login=? AND password=? ";
    private final static String GET_USER = "SELECT * FROM user WHERE idUser=?";
    private static volatile MySQLLoginDAO instance;
    private static ReentrantLock lock = new ReentrantLock();

    private MySQLLoginDAO() {

    }

    public static MySQLLoginDAO getInstance() {
        if (instance == null) {
            lock.lock();
            try {
                instance = new MySQLLoginDAO();
            } finally {
                lock.unlock();
            }
        }
        return instance;
    }

    @Override
    public LoginBean create(LoginBean entity) throws DAOException {
        return null;
    }

    @Override
    public LoginBean find(Integer key) throws DAOException {
        return null;
    }

    @Override
    public List<LoginBean> findAll() throws DAOException {
        return null;
    }

    @Override
    public boolean update(LoginBean entity) throws DAOException {
        return false;
    }

    @Override
    public boolean delete(LoginBean entity) throws DAOException {
        return false;
    }

    @Override
    public boolean checkUserLogin(LoginBean loginBean) throws DAOException {
        ConnectionPool pool = ConnectionPool.getInstance();
        ResultSet resultSet = null;
        boolean flag = false;
        try (
                Connection connection = pool.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(CHECK_USER);
        ) {
            preparedStatement.setString(1, loginBean.getLogin());
            preparedStatement.setString(2, loginBean.getPassword());
            resultSet = preparedStatement.executeQuery();
            flag = resultSet.next();
            if (flag) {
                loginBean.setId(resultSet.getInt(1));
                loginBean.getUser().setId(resultSet.getInt(2));
            }
        } catch (ConnectionPoolException e) {
            throw new DAOException("Problem with connection pool", e);
        } catch (SQLException e) {
            throw new DAOException("SQL exception in CHECK_USER query", e);
        } finally {
            try {
                resultSet.close();
            } catch (SQLException e) {
                throw new DAOException("Problem with closing ResultSet", e);
            }
        }
        return flag;
    }

    @Override
    public UserBean buildUserBean(LoginBean loginBean) throws DAOException {
        ConnectionPool pool = ConnectionPool.getInstance();
        ResultSet resultSet = null;
        try (
                Connection connection = pool.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(GET_USER);
        ) {
            preparedStatement.setInt(1, loginBean.getUser().getId());
            resultSet = preparedStatement.executeQuery();
            parseResultSetForBuildUser(resultSet, loginBean.getUser());
        } catch (ConnectionPoolException e) {
            throw new DAOException();
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            try {
                resultSet.close();
            } catch (SQLException e) {
                throw new DAOException();
            }
        }
        return loginBean.getUser();
    }

    private void parseResultSetForBuildUser(ResultSet resultSet, UserBean userBean) throws DAOException {
        try {
            resultSet.next();
            userBean.setName(resultSet.getString(2));
            userBean.setLastName(resultSet.getString(3));
            userBean.setPhone(resultSet.getString(4));
            userBean.setEmail(resultSet.getString(5));
            userBean.setPassportNumber(resultSet.getString(6));
            userBean.setRole(resultSet.getString(7));
        } catch (SQLException e) {
            throw new DAOException("Parse result set for user bean problem", e);
        }
    }
}
