package by.epam.training.task6.dao.connectionpool;

public enum DBContextParameter {
    DRIVER("db.driver"),
    URL("db.url"),
    USER("db.user"),
    PASSWORD("db.password"),
    POOL_SIZE("db.poolsize");
    private String value;

    DBContextParameter(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
