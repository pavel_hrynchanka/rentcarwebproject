package by.epam.training.task6.dao.connectionpool;

import java.util.ResourceBundle;

public class DBResourceManager {
    private final static String PROPERTY_FILE_NAME = "db";
    private final static DBResourceManager instance = new DBResourceManager();
    private ResourceBundle resourceBundle;

    private DBResourceManager() {
        resourceBundle = ResourceBundle.getBundle(PROPERTY_FILE_NAME);
    }

    public static DBResourceManager getInstance() {
        return instance;
    }

    public String getValue(String key) {
        return resourceBundle.getString(key);
    }
}
