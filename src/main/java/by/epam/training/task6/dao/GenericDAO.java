package by.epam.training.task6.dao;

import java.util.List;

public interface GenericDAO<T, K extends Integer> {

    T create(T entity) throws DAOException;

    T find(K key) throws DAOException;

    List<T> findAll() throws DAOException;

    boolean update(T entity) throws DAOException;

    boolean delete(T entity) throws DAOException;
}
