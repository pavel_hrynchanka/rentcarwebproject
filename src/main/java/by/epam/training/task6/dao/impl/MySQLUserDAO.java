package by.epam.training.task6.dao.impl;

import by.epam.training.task6.dao.DAOException;
import by.epam.training.task6.dao.UserDAO;
import by.epam.training.task6.dao.connectionpool.ConnectionPool;
import by.epam.training.task6.dao.connectionpool.ConnectionPoolException;
import by.epam.training.task6.domain.user.UserBean;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

public class MySQLUserDAO implements UserDAO {
    private final static String CREATE_USER = "INSERT INTO user(name,lastName,phone,email,passportNumber,role)values (?,?,?,?,?,'user')";
    private final static String GET_USER_ID = "SELECT idUser FROM user WHERE email=?";
    private final String USER_ROLE = "user";
    private static volatile MySQLUserDAO instance;
    private static ReentrantLock lock = new ReentrantLock();

    private MySQLUserDAO() {

    }

    public static MySQLUserDAO getInstance() {
        if (instance == null) {
            lock.lock();
            try {
                instance = new MySQLUserDAO();
            } finally {
                lock.unlock();
            }
        }
        return instance;
    }

    @Override
    public UserBean create(UserBean entity) throws DAOException {
        ConnectionPool pool = ConnectionPool.getInstance();
        UserBean viewObject = null;
        try (
                Connection connection = pool.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(CREATE_USER);
        ) {
            prepareStatementForInsert(preparedStatement, entity);
            int rowCount = preparedStatement.executeUpdate();
            if (rowCount == 1) {
                viewObject = entity;
                viewObject.setRole(USER_ROLE);
                setUserId(viewObject);
            } else {
                throw new DAOException("Was created more than one user!");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ConnectionPoolException e) {
            e.printStackTrace();
        }
        return viewObject;
    }

    @Override
    public UserBean find(Integer key) throws DAOException {
        return null;
    }

    @Override
    public List<UserBean> findAll() throws DAOException {
        return null;
    }

    @Override
    public boolean update(UserBean entity) throws DAOException {
        return false;
    }

    @Override
    public boolean delete(UserBean entity) throws DAOException {
        return false;
    }

    private void prepareStatementForInsert(PreparedStatement statement, UserBean userBean) throws SQLException {
        statement.setString(1, userBean.getName());
        statement.setString(2, userBean.getLastName());
        statement.setString(3, userBean.getPhone());
        statement.setString(4, userBean.getEmail());
        statement.setString(5, userBean.getPassportNumber());
    }

    @Override
    public void setUserId(UserBean userBean) throws DAOException {
        ConnectionPool pool = ConnectionPool.getInstance();
        ResultSet resultSet = null;
        try (
                Connection connection = pool.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(GET_USER_ID);
        ) {
            preparedStatement.setString(1, userBean.getEmail());
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                userBean.setId(resultSet.getInt(1));
            } else {
                throw new DAOException("There is no user in system");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ConnectionPoolException e) {
            e.printStackTrace();
        }
    }
}
