package by.epam.training.task6.service;

import by.epam.training.task6.dao.DAOException;
import by.epam.training.task6.dao.LoginDAO;
import by.epam.training.task6.dao.impl.MySQLLoginDAO;
import by.epam.training.task6.domain.login.LoginBean;
import by.epam.training.task6.domain.user.UserBean;

import java.util.concurrent.locks.ReentrantLock;

public final class LoginServiceImpl implements Service<LoginBean, UserBean> {
    private static volatile LoginServiceImpl instance;
    private static ReentrantLock lock = new ReentrantLock();

    private LoginServiceImpl() {
    }

    public static LoginServiceImpl getInstance() {
        if (instance == null) {
            lock.lock();
            try {
                instance = new LoginServiceImpl();
            } finally {
                lock.unlock();
            }
        }
        return instance;
    }

    @Override
    public UserBean executeService(LoginBean transferObject) throws ServiceException {
        LoginDAO loginDAO = MySQLLoginDAO.getInstance();
        UserBean viewLoginObject = null;
        //validate
        try {
            boolean isCheck = loginDAO.checkUserLogin(transferObject);
            if (isCheck) {
                viewLoginObject = loginDAO.buildUserBean(transferObject);
            }
        } catch (DAOException e) {
            throw new ServiceException("Problem with LoginDAO",e);
        }
        return viewLoginObject;
    }
}
