package by.epam.training.task6.service;

public interface Service<TO, VO> {
    VO executeService(TO transferObject) throws ServiceException;
}
