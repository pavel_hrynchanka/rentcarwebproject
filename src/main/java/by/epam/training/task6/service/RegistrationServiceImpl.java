package by.epam.training.task6.service;

import by.epam.training.task6.dao.DAOException;
import by.epam.training.task6.dao.UserDAO;
import by.epam.training.task6.dao.impl.MySQLUserDAO;
import by.epam.training.task6.domain.user.UserBean;

import java.util.concurrent.locks.ReentrantLock;

public final class RegistrationServiceImpl implements Service<UserBean, UserBean> {
    private static volatile RegistrationServiceImpl instance;
    private static ReentrantLock lock = new ReentrantLock();

    private RegistrationServiceImpl() {

    }

    public static RegistrationServiceImpl getInstance() {
        if (instance == null) {
            lock.lock();
            try {
                instance = new RegistrationServiceImpl();
            } finally {
                lock.unlock();
            }
        }
        return instance;
    }

    @Override
    public UserBean executeService(UserBean transferObject) throws ServiceException {
        UserDAO userDAO = MySQLUserDAO.getInstance();
        UserBean viewObject = null;
        try {
            //валидация
            viewObject = userDAO.create(transferObject);
        } catch (DAOException e) {
            e.printStackTrace();
        }
        return viewObject;
    }
}
