package by.epam.training.task6.dao.impl;

import by.epam.training.task6.dao.DAOException;
import by.epam.training.task6.dao.LoginDAO;
import by.epam.training.task6.dao.connectionpool.ConnectionPoolException;
import by.epam.training.task6.dao.connectionpool.ConnectionPoolTest;
import by.epam.training.task6.domain.login.LoginBean;
import by.epam.training.task6.domain.user.UserBean;
import org.junit.Assert;
import org.junit.Test;

public class MySQLLoginDAOTest {
    @Test
    public void checkUserTest() throws DAOException, ConnectionPoolException {
        ConnectionPoolTest.initPoolTest();
        LoginBean loginBean = new LoginBean();
        loginBean.setLogin("admin");
        loginBean.setPassword("admin");
        LoginDAO loginDAO = MySQLLoginDAO.getInstance();
        boolean isCheck = loginDAO.checkUserLogin(loginBean);
        System.out.println(loginBean.getId() + " " + loginBean.getUser().getId());
        Assert.assertEquals(true, isCheck);
        ConnectionPoolTest.destroyPoolTest();
    }

    @Test
    public void buildUserBeanTest() throws ConnectionPoolException, DAOException {
        ConnectionPoolTest.initPoolTest();
        LoginBean loginBean = new LoginBean();
        loginBean.setId(1);
        loginBean.getUser().setId(1);
        LoginDAO loginDAO = MySQLLoginDAO.getInstance();
        UserBean userBean = loginDAO.buildUserBean(loginBean);
        System.out.println(userBean.toString());
        ConnectionPoolTest.destroyPoolTest();
    }
}
