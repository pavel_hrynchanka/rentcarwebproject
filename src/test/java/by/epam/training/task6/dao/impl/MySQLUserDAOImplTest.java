package by.epam.training.task6.dao.impl;

import by.epam.training.task6.dao.DAOException;
import by.epam.training.task6.dao.UserDAO;
import by.epam.training.task6.dao.connectionpool.ConnectionPoolException;
import by.epam.training.task6.dao.connectionpool.ConnectionPoolTest;
import by.epam.training.task6.domain.user.UserBean;
import org.junit.Test;

public class MySQLUserDAOImplTest {
    @Test
    public void createTest() throws DAOException, ConnectionPoolException {
        ConnectionPoolTest.initPoolTest();
        UserBean userBean = new UserBean();
        userBean.setName("Alex228");
        userBean.setLastName("Smith");
        userBean.setPhone("9929992");
        userBean.setEmail("alex@gmail.com");
        userBean.setPassportNumber("228228228");
        UserBean to = null;
        UserDAO userDAO = MySQLUserDAO.getInstance();
        to = userDAO.create(userBean);
        if (to != null){
            System.out.println(to.toString());
        }
        ConnectionPoolTest.destroyPoolTest();
    }
}
