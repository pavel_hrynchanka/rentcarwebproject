package by.epam.training.task6.dao.connectionpool;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ConnectionPoolTest {
    @BeforeClass
    public static void initPoolTest() throws ConnectionPoolException {
        ConnectionPool pool = ConnectionPool.getInstance();
        pool.init();
    }

    @Test
    public void testSQLStatement() throws ConnectionPoolException {
        ConnectionPool pool = ConnectionPool.getInstance();
        try (
                Connection connection = pool.getConnection();
                Statement st = connection.createStatement();
                ResultSet rs = st.executeQuery("SELECT * FROM user");
        ) {
            while (rs.next()) {
                System.out.println(rs.getInt(1) + " " + rs.getString(2) + " " + rs.getString(3) + " " + rs.getString(4) + " " + rs.getString(5)
                        + " " + rs.getString(6) + " " + rs.getString(7));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ConnectionPoolException e) {
            e.printStackTrace();
        } finally {
            pool.destroy();
        }

    }

    @AfterClass
    public static void destroyPoolTest() throws ConnectionPoolException {
        ConnectionPool pool = ConnectionPool.getInstance();
        pool.destroy();
    }
}
