package by.epam.training.task6.dao.controller.command.implementation;

import by.epam.training.task6.controller.command.CommandException;
import by.epam.training.task6.dao.connectionpool.ConnectionPoolException;
import by.epam.training.task6.dao.connectionpool.ConnectionPoolTest;
import org.junit.Test;

public class LoginCommandImplTest {
    @Test
    public void executeTest() throws CommandException, ConnectionPoolException {
        ConnectionPoolTest.initPoolTest();
        String login = "admin";
        String pwd = "admin";
//        String page = new LoginCommandImpl().execute(login,pwd);
//        System.out.println(page);
        ConnectionPoolTest.destroyPoolTest();
    }
}
